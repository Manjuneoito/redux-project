
import Footer from "./page/section/Footer";
import Header from "./page/section/Header";
import Main from "./page/section/Main";

function App() {
 
  return (
    <div className="w-full h-screen">
    <Header />
    <Main />
    <Footer />
    </div>
  );
}

export default App;
