import { useSelector, useDispatch } from "react-redux";
import { increase_count, decrease_count } from "../../redux/count/countSlice";

function Counter() {
  const count = useSelector((state) => state.count.value);
  const dispatch = useDispatch();

  const increment = () => {
    dispatch(increase_count());
  };
  const decrement = () => {
    dispatch(decrease_count());
  };
  return (
    <div className="border-1 border-black shadow px-52 py-3 m-3">
      <h1 className="text-xl p-8 text-blue-600">
        {" "}
        The Count is :{count} <span className="text-black text-3xl"></span>
      </h1>
      <div>
        <button
          onClick={increment}
          className="bg-blue-600 p-3 m-3 rounded text-white"
        >
          Increase
        </button>
        <button
          onClick={decrement}
          className="bg-blue-600 p-3 m-3 rounded text-white"
        >
          Decrease
        </button>
      </div>
    </div>
  );
}

export default Counter;
