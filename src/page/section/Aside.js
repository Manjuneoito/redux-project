import randomColor from "randomcolor";
import { useSelector, useDispatch } from "react-redux";
import { change_color } from "../../redux/color/colorSlice";

function Aside() {
  const color = useSelector((state) => state.color.value);
  const dispatch = useDispatch();
  const change = () => {
    dispatch(
      change_color({
        color: randomColor(),
      })
    );
  };
  return (
    <div className="border-1 border-black shadow px-36 py-3 m-3">
      <h1 className=" p-8 text-xl" style={{ color }}>
        Change color Randomly
      </h1>
      <button
        onClick={change}
        className="bg-blue-600 p-3 m-3 rounded text-white"
      >
        Change Colour
      </button>
    </div>
  );
}

export default Aside;
