import react from "react";
import Aside from "./Aside";
import Counter from "./Counter";

function Main() {
  return (
    <div className="border-1 border-black shadow py-24 px-5 m-3 flex justify-between">
      <Counter />
      <Aside />
    </div>
  );
}

export default Main;
