import React from "react";
import { useSelector } from "react-redux";

function Footer() {
  const color = useSelector((state) => state.color.value);
  return (
    <div className="border-1 border-black shadow p-5 m-3">
      <h1 className="text-center p-5 text-xl" style={{ color }}>
        This Is Footer
      </h1>
    </div>
  );
}

export default Footer;
