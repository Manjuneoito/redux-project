import react from "react";
import { useSelector } from "react-redux";
function Header() {
  const color = useSelector((state) => state.color.value);
  const count = useSelector((state) => state.count.value);
  return (
    <div className="border-1 border-black shadow p-5 m-3 flex justify-between">
      <h1 className="bg-white  text-xl" style={{ color }}>
        My Redux Project
      </h1>
      <h1 className="bg-white text-blue-600 text-xl">Count :{count} </h1>
    </div>
  );
}

export default Header;
